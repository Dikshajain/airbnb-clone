import React from 'react';
import './SearchPage.css';
import SearchResult from './SearchResult';
import {Button} from '@material-ui/core';
import banner from '../src/images/banner.jpg';


function SearchPage() {
    return (
        <div className="searchpage">
            <div className="searchPage__info">
                <p>62 stays - 26 august to 30 august - 2 guest</p>
                <h1>Stays nearby</h1>
                <Button variant="outlined">Cancellation Flexibility</Button>
                <Button variant="outlined">Type of place</Button>
                <Button variant="outlined">Price</Button>
                <Button variant="outlined">Rooms and beds</Button>
                <Button variant="outlined">More filters</Button>
            </div>
            <SearchResult img={banner}
                            location="Private room in center of london"
                           title="Stay at this spacious Edwardian House"
                           description="1 guest . 1 bedroom -wifi- kitchen - Free parking . Washing Machine"
                           star={4.73}
                           price="$30 / night"
                           total="$117 total"     />
                             <SearchResult img={banner}
                            location="Private room in center of london"
                           title="Stay at this spacious Edwardian House"
                           description="1 guest . 1 bedroom -wifi- kitchen - Free parking . Washing Machine"
                           star={4.73}
                           price="$30 / night"
                           total="$117 total"     />

<SearchResult img={banner}
                            location="Private room in center of london"
                           title="Stay at this spacious Edwardian House"
                           description="1 guest . 1 bedroom -wifi- kitchen - Free parking . Washing Machine"
                           star={4.73}
                           price="$30 / night"
                           total="$117 total"     />
        </div>
    )
}

export default SearchPage;
