import React from 'react';
import './Home.css';
import Banner from './Banner';
import Card from './Card';
import banner from '../src/images/banner.jpg';

function Home() {
    return (
        <div className="home">
            <Banner />

            <div className="home__section">
                <Card src={banner}
                    title="Entire homes"
                    description="Comfortable private places,with rooms for friends and family."
                    />
                <Card src={banner}
                    title="Online Experience"
                    description="Unique activities we can do together,led by a world of hosts."
                    />
                <Card src={banner}
                    title="Unique Stays" 
                    description="Spaces that are more than just a place to sleep."/>
                
            </div>
            <div className="home__section">
            <Card src={banner}
                    title="Entire homes"
                    
                    description="Comfortable private places,with rooms for friends and family."
                    price="$130/night"
                    />
                <Card src={banner}
                    title="Online Experience"
                    description="Unique activities we can do together,led by a world of hosts."
                    price="$130/night"
                    />
                <Card src={banner}
                    title="Unique Stays" 
                    description="Spaces that are more than just a place to sleep."
                     price="$130/night"/>
            </div>
        </div>
    )
}

export default Home;
